alter table employees
add country varchar(100);

alter table employees
add bDate date;

alter table employees
modify column bDate year;

alter table employees
drop column country;

truncate table orderdetails;


