CREATE TABLE `employees_backup`(
`id` INT AUTO_INCREMENT,
`Employee_ID` int(11) NOT NULL,
`LastName` varchar(45) NOT NULL,
`FirstName` varchar(45) NOT NULL,
`BirthDate` varchar(45) NOT NULL,
`changedat` DATETIME DEFAULT NULL,
`action` VARCHAR(50) DEFAULT NULL,
PRIMARY KEY (`id`)
);

select * from employees_backup;

update employees set LastName = "ÇETINER" where EmployeeID = 6;
update employees set BirthDate = "1997-04-01" where EmployeeID = 8;

insert into employees (EmployeeID,LastName,FirstName,BirthDate,Photo,Notes)
		values ("11","CETINER","ELIF","1997-04-01","EmpID11.pic","Intern");
        
delete from employees where EmployeeID = 11;