show columns from customers ;

select *  
from customers
order by CustomerName;

describe customers;
create index index2
on customers(CustomerName);

explain select * 
from customers;
alter table customers drop index2;

create view tmp as select *
 from customers
order by CustomerName;

select *
from tmp;

insert into tmp values(9,'efkfjs','fxlfxv','sdsd','sdgsjlg','sdkguhskgfs','sdfguhsb');

